# Downsmash

The [Downsmash](downsma.sh) project intends to catalog and document as much of the history of competitive Melee as possible.

In service of this goal, we're developing:

- [downsmash/segment](https://github.com/downsmash/segment), which watches Melee VODs and returns the approximate start and end times of contiguous matches.
- [downsmash/corpus](https://github.com/downsmash/corpus), which generates training images of, e.g., ports, from [Project Slippi](https://slippi.gg) replay files.
- [downsmash/titles](https://github.com/downsmash/titles), which searches YouTube for Melee videos and trains a classifier on the relevant information (players, characters, tournament, round, etc.)
